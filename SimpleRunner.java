// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.merge.ConcurrentMerge;
import com.deltaxml.mergecommon.config.*;
import com.deltaxml.merge.ThreeWayMerge.ResultFormat;
import com.deltaxml.merge.SequentialMerge;
import com.deltaxml.merge.ThreeWayMerge;

import java.io.File;

public class SimpleRunner {
  public static void main(String[] args) throws Exception {
    File dataFolder = new File("html-data/");
    File samplesResultFolder = new File("results/");

    // Ancestor file
    File ancestorFile = new File(dataFolder, "four-edits.html");

    // Version files
    File annaVersion = new File(dataFolder, "four-edits-anna.html");
    File benVersion = new File(dataFolder, "four-edits-ben.html");
    File chrisVersion = new File(dataFolder, "four-edits-chris.html");
    File davidVersion = new File(dataFolder, "four-edits-david.html");

    // Result File
    File resultFile = new File("result.xml");

    System.out.println("Running Concurrent Merge....");
    ConcurrentMerge merger= new ConcurrentMerge();

    // Set this to true to pretty print the merge output
    merger.setIndent(false);

    merger.setAncestor(ancestorFile, "master");
    System.out.println("Ancestor (\"master\") set");

    merger.addVersion(annaVersion, "anna");
    System.out.println("Version \"anna\" added");

    merger.addVersion(benVersion, "ben");
    System.out.println("Version \"ben\" added");

    merger.addVersion(chrisVersion, "chris");
    System.out.println("Version \"chris\" added");

    merger.addVersion(davidVersion, "david");
    System.out.println("Version \"david\" added");

    merger.setResultType(com.deltaxml.mergecommon.config.ConcurrentMergeResultType.DELTAV2);
    merger.extractAll(new File(samplesResultFolder, "merge-result-deltav2.xml"));
    merger.setResultType(com.deltaxml.mergecommon.config.ConcurrentMergeResultType.ANALYZED_DELTAV2);
    merger.extractAll(new File(samplesResultFolder, "merge-result-analyzed.xml"));
    merger.setResultType(com.deltaxml.mergecommon.config.ConcurrentMergeResultType.RULE_PROCESSED_DELTAV2);
    merger.extractAll(new File(samplesResultFolder, "merge-result-processed.xml"));
    merger.setResultType(com.deltaxml.mergecommon.config.ConcurrentMergeResultType.SIMPLIFIED_DELTAV2);
    merger.extractAll(new File(samplesResultFolder, "merge-result-simplified-delta.xml"));
    merger.setResultType(com.deltaxml.mergecommon.config.ConcurrentMergeResultType.SIMPLIFIED_RULE_PROCESSED_DELTAV2);
    merger.extractAll(new File(samplesResultFolder, "merge-result-simplified-rule-processed-delta.xml"));

    System.out.println("Concurrent Merge Result files created at: " + samplesResultFolder.getCanonicalPath());
    System.out.println("");

    // We could also construct a RuleConfiguration object here and do more specific rule processing.

    // Now using the three way merger we will generate a variety of three way results.
    // The use of 'presets' is demonstrated in the 'ThreeToTwoMergeUseCases' sample
    System.out.println("Running Three Way Concurrent Merge....");
    ThreeWayMerge tw= new ThreeWayMerge();
    tw.setThreeWayResultType(ThreeWayMergeResultType.THREE_WAY_OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File(samplesResultFolder, "result-three-way-oxygen-track-changes.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.XML_DELTA);
    tw.merge(ancestorFile, annaVersion, benVersion, new File(samplesResultFolder, "result-two-way-xml-delta.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File(samplesResultFolder, "result-two-way-oxygen-track-changes.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.RULE_PROCESSED_TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.XML_DELTA);
    tw.merge(ancestorFile, annaVersion, benVersion, new File(samplesResultFolder, "result-rule-processed-two-way-xml-delta.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File(samplesResultFolder, "result-rule-processed-two-way-oxygen-track-changes.xml"));

    System.out.println("Three Way Concurrent Merge Result files created at: " + samplesResultFolder.getCanonicalPath());
    System.out.println("");

    System.out.println("Running Sequential Merge....");
    SequentialMerge seqMerger= new SequentialMerge();

    //there are other addVersion methods which can take list of files
    seqMerger.addVersion(ancestorFile, "master");
    System.out.println("First version (\"original\") set");

    seqMerger.addVersion(annaVersion, "anna");
    System.out.println("Version \"anna\" added");

    seqMerger.addVersion(benVersion, "ben");
    System.out.println("Version \"ben\" added");

    seqMerger.addVersion(chrisVersion, "chris");
    System.out.println("Version \"chris\" added");

    seqMerger.addVersion(davidVersion, "david");
    System.out.println("Version \"david\" added");

    seqMerger.setResultType(com.deltaxml.mergecommon.config.SequentialMergeResultType.DELTAV2);
    seqMerger.extractAll(new File(samplesResultFolder, "seq-merge-result-deltav2.xml"));
    seqMerger.setResultType(com.deltaxml.mergecommon.config.SequentialMergeResultType.ANALYZED_DELTAV2);
    seqMerger.extractAll(new File(samplesResultFolder, "seq-merge-result-analyzed.xml"));
    seqMerger.setResultType(com.deltaxml.mergecommon.config.SequentialMergeResultType.SIMPLIFIED_DELTAV2);
    seqMerger.extractAll(new File(samplesResultFolder, "seq-merge-result-simplified-delta.xml"));

    System.out.println("Sequential Merge Result files created at: " + samplesResultFolder.getCanonicalPath());
  }
}
